#!/bin/bash

fail() {
    echo '[FAILED] ' "$@" >&2
    exit 1
}

[ "$#" -eq 1 ] || fail "usage: $0 <template-file>"
[ -f "$1" ] || fail "template file does not exists: $1"


while IFS= read -r line <&3; do
	if egrep -i '^##[-a-z0-9]+##$' <<< "${line}" >/dev/null 2>&1; then
		mixin=$(cut -d '#' -f 3 <<< "${line}")

		if [ ! -f "images/mixins/${mixin}" ]; then
			fail "mixin '${mixin}' not found"
		fi

		cat "images/mixins/${mixin}"
	elif egrep '^#!>' <<< "${line}" >/dev/null 2>&1; then
		cmd=$(tail -c +4 <<< "${line}")
		eval "echo $cmd"
	elif egrep '^#!' <<< "${line}" >/dev/null 2>&1; then
		cmd=$(tail -c +4 <<< "${line}")
		eval "$cmd"
	else
		echo "${line}"
	fi

done 3< "$1"

