#!/bin/bash

APP=$1
DEST_DIR=$2

cd ./containments/
  set -a
    eval . $APP
  set +a
cd ..

export APP

./scripts/fill-template.sh ./scripts/application-entry.template > $DEST_DIR/$APP.desktop