#!/bin/bash

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

CLI=podman
APP=$1

RUN_DIR=$(mktemp -d --suffix=.container)

cd "${SCRIPT_DIR}/../containments/"
. $APP

OPTS+=(
	"-u 1000:1000"
	"-w /home/user"
)

fail() {
    echo "$@" >&2
    exit 1
}

for ((i = 0; i < ${#BEFORE[@]}; i++))
do
    eval ${BEFORE[$i]}
    [ $? -eq 0 ] || fail "command '${BEFORE[$i]}' failed!"
done

cleanup() {
	for ((i = 0; i < ${#AFTER[@]}; i++))
	do
	    eval ${AFTER[$i]}
	done
	if [ ! -z ${RUN_DIR+x} ]; then
		rm -rf "$RUN_DIR"
	fi
}

host_commands() {
	if [ ! -z ${HOST_CMD+x} ]; then
		eval ${HOST_CMD}
	fi
}

if ${CLI} container exists $APP; then
  fail "$APP already running"
fi

host_commands &

${CLI} run \
  --rm=true \
  --interactive \
  --tty \
  --pull=never \
  --replace=true \
  --security-opt=seccomp=unconfined \
  --security-opt=apparmor=unconfined \
  --net=${NETWORK:-none} \
  --pid=${PID:-private} \
  --userns=${USERNS:-keep-id} \
  --uts=${UTSNS:-private} \
  --name=$APP \
  ${EXPOSE[@]/#/-v } \
  ${VOLUMES[@]/#/-v } \
  ${ENVIRONMENT[@]/#/-e } \
  ${OPTS[@]} \
  localhost/$CONTAINER \
  ${OVERRIDE_CMD:-${CMD}}

wait

cleanup

