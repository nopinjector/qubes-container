#!/bin/bash

while [ $# -gt 0 ]; do
	case $1 in
		update)
			apt-get update >/dev/null 2>&1 
			;;
		read)
			UPDATES=$(apt-get dist-upgrade -s -V -qq | egrep '^Inst')
			;;
		list-verbose)
			echo -n "${UPDATES}"
			;;
		list)
			echo -n "${UPDATES}" | perl -n -e'/^Inst ([^ ]+) / && print "$1\n"'
			;;
		count)
			echo "$(echo -n "${UPDATES}" | wc -l) packages with updates"
			;;
	esac
	shift
done
