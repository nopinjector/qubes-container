# qubes-container

**Disclaimer**: *This project is not affiliated with the Qubes OS project in any way. It was made by me for my personal Qubes OS based system setup.*

## Qubes OS

Qubes OS is a Linux distribution focusing on security by implementing strong isolation via virtualization. By its awesome template system, it gives users the possibility to use plenty of different isolated VM instances in parallel without spending to much effort in configuring and keeping the base system up to date.

More info about [Qubes OS](https://www.qubes-os.org/)

# Why qubes-container

Whereas the template system and the isolation is sufficient in many cases, I want to further isolate apps running inside a Qube and 'micro-template' my applications. One major initial reason for the creation of this project was the rationale about [trusting installers in a TemplateVM](https://www.qubes-os.org/doc/templates/#trusting-your-templatevms):

> As the TemplateVM is used for creating filesystems for other AppVMs where you actually do the work, it means that the TemplateVM is as trusted as the most trusted AppVM based on this template. In other words, if your template VM gets compromised, e.g. because you installed an application, whose installer’s scripts were malicious, then all your AppVMs (based on this template) will inherit this compromise.

*from [QubesOS : TemplateVMs](https://www.qubes-os.org/doc/templates/#trusting-your-templatevms)*

As not living in a ideal world (in which Qubes OS would not be required), trust, especially in third party closed source binary installers, is not ultimate (read this as somewhere between 'not existing' and 'somewhat present'). As all installers running in a TemplateVM can comprise the whole template (and therefore all AppVM based on it), further segmentation is desired. Qubes OS website recommends the use of StandaloneVMs and multiple TemplateVMs for different classes of trust levels. Both suggestions have the downside of increased maintenance efforts for the Qubes (updates, configuration and so on), especially StandaloneVMs.

Furthermore, every instantiated Qube occupies ressources on a system and decreases the performance of the overall system. This is especially true for notebooks (my primary work environment). Having more and fine granular segmentation, the scope of a possible infection is more limited and it is easier to argue about it. By levering container isolation, whereas definitly not providing the same security as the virtualization based isolation of Qubes, it provides further *micro-segmentation* with somewhat reasonable security and good performance.

# Legal

Some container build recipes download and install commercial third party software. **Please ensure to read their license terms before usage!**

## TODO

*more to come*
