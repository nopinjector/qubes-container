#!/bin/bash

get_version() {
  # AUR package from ArchLinux seems to be up to date with Slack releases
  # Read current package version from there
  url="https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=slack-desktop"
  curl -L -s -S "${url}" \
      | grep -E '^pkgver=' \
      | perl -n -e'/=([0-9.]+)\s*$/ && print $1'
}

download() {
  url="https://downloads.slack-edge.com/linux_releases/slack-desktop-$1-amd64.deb"
  curl ${CURL_OPTIONS} -o "$2" "${url}"
}

fail() {
  echo $1 >&2; exit 1
}

[ "$#" -eq 2 ] || fail "usage: $0 <action> <dest-file>"
ACTION="$1"
DEST_FILE="$2"

VERSION=$(get_version)

case ${ACTION} in
  version)
    echo ${VERSION}
    ;;
  download)
    download "${VERSION}" "${DEST_FILE}"
    ;;
  check)
    # 'content_type' would be another possibility: should be application/octet-stream
    STATUS=$(CURL_OPTIONS="-I -s -w %{response_code}" download "${VERSION}" "${DEST_FILE}")
    [ "${STATUS}" == "200" ] || fail "download failed"
    ;;
esac