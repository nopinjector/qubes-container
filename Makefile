#!/usr/bin/make -f

include config

CLI=podman
APPS=$(foreach app,$(wildcard images/app-*), $(subst images/,,$(app)))
APP_QUBES=$(foreach app,$(APPS), $(IMAGE_PREFIX)-$(app))
CONTAINMENTS=$(foreach containment,$(wildcard containments/app-*), $(subst containments/,,$(containment)))

all: $(APPS)

%/shortcut:
ifndef APPLICATIONS_PATH
	@echo "variable APPLICATIONS_PATH not set! exiting..." >&2 && false
endif
ifndef BASE_DIR
	@echo "variable BASE_DIR not set! exiting..." >&2 && false
endif
	BASE_DIR=$(BASE_DIR) ./scripts/create-desktop-entry.sh "$(subst /shortcut,,$@)" "$(APPLICATIONS_PATH)"

shortcuts: $(foreach containment,$(CONTAINMENTS), $(containment)/shortcut)

%/exist:
	@$(CLI) image exists localhost/$(subst /exist,,$@) || echo "$(subst /exist,,$@) does not exist"

exist: $(foreach app,$(APP_QUBES), $(app)/exist)

clean:
	find . -name .docker-build -print -delete

%/hardclean:
	-$(CLI) image exists localhost/$(subst /hardclean,,$@) && $(CLI) image rm --force $(subst /hardclean,,$@)

hardclean: clean $(foreach app,$(APP_QUBES), $(app)/hardclean)

sparklingclean: clean
	$(CLI) image rm --force --all

%/check:
	@echo -n "$(subst /check,,$@): "; $(CLI) run --rm -v ./scripts/check-updates.sh:/check.sh:ro $(subst /check,,$@) /check.sh update read count

%/list:
	@$(CLI) run --rm -v ./scripts/check-updates.sh:/check.sh:ro $(subst /list,,$@) /check.sh update read list-verbose

check: $(foreach app,$(APP_QUBES), $(app)/check)


%/fastcheck:
	@echo -n "$(subst /fastcheck,,$@): "; $(CLI) run --rm \
		-v ./.cache/fastcheck-aptlist:/var/lib/apt/lists:Z \
		-v ./scripts/check-updates.sh:/check.sh:ro \
		$(subst /fastcheck,,$@) /check.sh read count

fastcheck-prepare:
	@mkdir -p ./.cache/fastcheck-aptlist
	@$(CLI) run --rm \
		-v ./scripts/sources.additional.d/lists:/etc/apt/sources.list.d:ro \
		-v ./scripts/sources.additional.d/keys:/etc/apt/trusted.gpg.d:ro \
		-v ./.cache/fastcheck-aptlist:/var/lib/apt/lists:rw \
		--userns keep-id \
		qubes-base-debian apt-get update >/dev/null 2>&1

fastcheck-cleanup:
	rm -rf ./.cache/fastcheck-aptlist

fastcheck: fastcheck-prepare \
		$(foreach app,$(APP_QUBES), $(app)/fastcheck) \
		fastcheck-cleanup

%/update:
	echo $(subst /update,,$@):latest | \
		./scripts/fill-template.sh scripts/update-template | \
		$(CLI) build -t $(subst /update,,$@) -f -

update: $(foreach app,$(APP_QUBES), $(app)/update)

%/pull:
	$(CLI) pull $(subst ^,:,$(subst /pull,,$@))

%/copy-conf:
	@[ -d conf/$(subst /copy-conf,,$@) ] || (echo 'conf/$(subst /copy-conf,,$@) does not exist' >&2 && false)
	mkdir -p images/app-$(subst /copy-conf,,$@)/conf
	cp -r conf/$(subst /copy-conf,,$@)/. images/app-$(subst /copy-conf,,$@)/conf/

%/Dockerfile: %/Dockerfile.in
	./scripts/fill-template.sh $< > $@

%/build: images/%/Dockerfile
	$(CLI) build -t "$(IMAGE_PREFIX)-$(subst /build,,$@)" images/$(subst /build,,$@)

%/compile: build/%/Dockerfile \
	   meta-builder
	if ! $(CLI) image exists "build-$(subst /compile,,$@)"; then \
	if [ -d "./.cache/build/$(subst /compile,,$@)" ]; then \
		echo "FROM scratch\nCOPY . /artifacts" | \
			$(CLI) build -t build-$(subst /compile,,$@) -f - ./.cache/build/$(subst /compile,,$@)/artifacts;  \
	else \
		$(CLI) build -t "build-$(subst /compile,,$@)" build/$(subst /compile,,$@); \
		$(CLI) create --name "container-$(subst /compile,,$@)" "build-$(subst /compile,,$@)"; \
		mkdir -p "./.cache/build/$(subst /compile,,$@)"; \
		$(CLI) cp "container-$(subst /compile,,$@):/artifacts" "./.cache/build/$(subst /compile,,$@)"; \
		$(CLI) rm "container-$(subst /compile,,$@)"; \
	fi \
	fi

app-audacity: base-gui \
              app-audacity/build

app-androidstudio: base-gui \
                   androidstudio/compile \
                   app-androidstudio/build

app-burp: base-gui \
          burpsuite/compile \
          app-burp/build

app-chrome: base-google \
            app-chrome/build

app-discord: base-gui \
             meta-builder \
             app-discord/build

app-filezilla: base-gui \
               app-filezilla/build

app-firefox: base-gui \
             firefox/copy-conf \
             app-firefox/build

app-gimp: base-gui \
          app-gimp/build

app-imageviewer: base-gui \
                 app-imageviewer/build

app-inkscape: base-gui \
              app-inkscape/build

app-jetbrains: base-gui \
               jetbrains/compile \
               app-jetbrains/build

app-keepass: base-gui \
             app-keepass/build

app-latex: base-gui \
           app-latex/build

app-libreoffice: base-gui \
                 app-libreoffice/build

app-pdfviewer: base-gui \
               app-pdfviewer/build

app-qalculate: base-gui \
               app-qalculate/build

app-slack: base-gui \
           meta-builder \
           app-slack/build

app-softmaker: base-gui \
               softmaker/compile \
               app-softmaker/build

app-thunderbird: base-gui \
                 app-thunderbird/build

app-vlc: base-gui \
         app-vlc/build

app-vscode: base-microsoft \
            app-vscode/build

app-wireshark: base-gui \
               app-wireshark/build


base-debian: debian^testing/pull \
             base-debian/build \
             $(IMAGE_PREFIX)-base-debian/update

base-dev: base-debian \
          base-dev/build

base-google: base-gui \
             base-google/build

base-gui: base-debian \
          base-gui/build

base-microsoft: base-gui \
                base-microsoft/build


dev-golang: base-dev \
            dev-golang/build

dev-java: base-dev \
          dev-java/build

dev-native: base-dev \
            dev-native/build

dev-python: base-dev \
            dev-python/build

dev-rust: base-dev \
          dev-rust/build


meta-builder: debian^testing/pull \
              meta-builder/build \
              $(IMAGE_PREFIX)-meta-builder/update

